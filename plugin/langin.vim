au BufReadPre,BufNewFile *.* call LoadLanguage()
let g:package_dir = get(g:, 'package_dir', "pack")

function LoadLanguage()
  filetype detect
  let l:files =  split(system("ls $HOME/.vim/" . g:package_dir . "/" . &filetype . "/opt"))
  for l:file in l:files
    execute "packadd " . l:file
  endfor
  echom "Loaded plugins for " . &filetype
endfunction
