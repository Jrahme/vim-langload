# Vim LangLoad
A plugin to automatically load language specific plugins via the native Vim package manager

## Installation

Simply clone the project to the start folder of your main vim package directory

`git clone git@gitlab.com:Jrahme/vim-langload.git $HOME/.vim/pack/core/start/`

### Requirements

* Vim 8.0+

## Usage

Create a directory in your vim package directory named after the filetype the plugins are meant for.

`mkdir -p $HOME/.vim/pack/clojure`

Populate that directory with the name of the filetype

`mkdir $HOME/.vim/pack/clojure/opt`

Install your language specific plugins to that directory

`git clone https://github.com/tpope/vim-fireplace.git $HOME/.vim/pack/clojure/opt`

Start up vim and it will automatically load the plugins for that filetype. 

### The Vim Package Directory

By default LangLoad assumes that your packages will be in the `$HOME/.vim/pack` directory. If you use a different runtimepath than `pack` you can set the `g:package_dir` variable in your `.vimrc` file to set the custom name.


